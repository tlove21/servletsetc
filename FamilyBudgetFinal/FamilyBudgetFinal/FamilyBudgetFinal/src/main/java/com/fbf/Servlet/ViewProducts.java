package com.fbf.Servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "/viewproducts", urlPatterns = {"/viewproducts"})
public class ViewProducts extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");

        int shopId = Integer.parseInt(request.getParameter("shopEntityList"));;
        int shopId2 = Integer.parseInt(request.getParameter("shopEntityList2"));;

        ProductsDao pd = ProductsDao.getInstance();
        ProductsDao pd2 = ProductsDao.getInstance();

        final List<ProductsEntity> listShops = pd.getProductShop(shopId);
        List<ProductsEntity> listShops2 = pd2.getProductShop(shopId2);

        ShopsDao s1name = ShopsDao.getInstance();
        String shopname1 = s1name.getShop(shopId).getShopName();

        ShopsDao s2name = ShopsDao.getInstance();
        String shopname2 = s2name.getShop(shopId2).getShopName();


        try (PrintWriter out = response.getWriter()) {
            /* Page output created from header, footer and template body to load all needed styles */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Family Budget: View Products</title>");
            out.println("<meta charset=\"UTF-8\">\n" +
                    "    <meta name=\"description\" content=\"Family Budget Final\">\n" +
                    "    <meta name=\"keywords\" content=\"HTML,CSS,XML,JavaScript,JAVA\">\n" +
                    "    <meta name=\"author\" content=\"Trevon Morris\">\n" +
                    "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=0.86, maximum-scale=3.0, minimum-scale=0.86\">\n" +
                    "    <link rel=\"stylesheet\" href=\"css/normalize.css\">\n" +
                    "    <link rel=\"stylesheet\" href=\"css/styles.css\">");
            out.println("</head>");
            out.println("<body>");
            out.println("<header id=\"page-header\">\n" +
                    "    <div class=\"flexheader\">\n" +
                    "        <a href=\"../FamilyBudgetFinal_war\" class=\"logoimg\" title=\"Go to the PHP Motors Page\">\n" +
                    "        <img src=\"img/morrisfamilybudgetlogo.png\" alt=\"Morris Family logo\">\n" +
                    "        </a>\n" +
                    "    </div>\n" +
                    "</header>\n" +
                    "<nav class=\"topnav\" id=\"menu_nav\">\n" +
                    "    <!-- start links-->\n" +
                    "    <ul><li><a href=\"../FamilyBudgetFinal_war/addusers\">User Add</a></li> <li><a href=\"../FamilyBudgetFinal_war/addproducts\">Product Add</a></li><li><a href=\"../FamilyBudgetFinal_war/addshops\">Shop Add</a></li>\n" +
                    "        <li><a href=\"../FamilyBudgetFinal_war/viewusers\">View User</a></li><li><a href=\"../FamilyBudgetFinal_war/viewproducts\">View Products</a></li><li><a href=\"../FamilyBudgetFinal_war/viewshops\">View Shops</a></li></ul>\n" +
                    "    <!-- end of links-->\n" +
                    "</nav>");
            out.println("<main id=\"main\">");
            out.println("<section id=\"maincontent\">");
            out.println("<h4>Product Listing Comparison:</h4>");
            out.println("</section>");
            out.println("<div id=\"dualcolumn\">");
            out.println("<section id=\"leftcolumn\">");
            out.println("Shop 1: " + shopname1);
            out.println("<table width=\"100%\">");
            out.println("<tr><td>Product</td><td>Price</td></tr>");
            listShops.forEach((ProductsEntity product) -> out.println("<tr><td>" + product.getProductName() + "</td>" + "<td>" + product.getProductPrice() + "</td></tr>"));
            out.println("</table>");
            out.println("</section>");
            out.println("<section id=\"rightcolumn\">");
            out.println("Shop 2: " + shopname2);
            out.println("<table width=\"100%\">");
            out.println("<tr><td>Product</td><td>Price</td></tr>");
            listShops2.forEach((ProductsEntity product) -> out.println("<tr><td>" + product.getProductName() + "</td>" + "<td>" + product.getProductPrice() + "</td></tr>"));
            out.println("</table>");
            out.println("</section>");
            out.println("</div>");
            out.println("</main>");
            out.println("<footer id=\"footer\">\n" +
                    "    <p>&copy Morris Family Budget, All rights reserved<br>\n" +
                    "        All images used are believed to be in \"Fair Use\". Please notify the author if any are not and they will be removed.\n" +
                    "        Last Updated December 2020 </p>\n" +
                    "</footer>");
            out.println("</body>");
            out.println("</html>");
        }catch(IOException e){
            //Nothing to display not necessary, but just ensuring for testing only
            e.printStackTrace();
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //This uses the template created for the page to allow styling.
        request.getRequestDispatcher("WEB-INF/templates/viewProducts.jsp").forward(request, response);

    }
}
