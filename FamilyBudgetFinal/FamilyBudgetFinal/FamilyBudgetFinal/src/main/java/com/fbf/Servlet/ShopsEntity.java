package com.fbf.Servlet;

import javax.persistence.*;

@Entity
@Table(name = "shops", schema = "familybudgetfinal")
public class ShopsEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;
    private String shopName;
    private String shopAddress;

    public ShopsEntity(int id, String shopName, String shopAddress) {
        super();
        this.id = id;
        this.shopName = shopName;
        this.shopAddress = shopAddress;
    }

    public ShopsEntity() {
    }

    public ShopsEntity(int id) {
        this.id = id;
    }

    public ShopsEntity(String shopName, String shopAddress) {
        this.shopName = shopName;
        this.shopAddress = shopAddress;
    }

    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "ShopName", nullable = true, length = 45)
    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    @Basic
    @Column(name = "ShopAddress", nullable = true, length = 45)
    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ShopsEntity that = (ShopsEntity) o;

        if (id != that.id) return false;
        if (shopName != null ? !shopName.equals(that.shopName) : that.shopName != null) return false;
        if (shopAddress != null ? !shopAddress.equals(that.shopAddress) : that.shopAddress != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (shopName != null ? shopName.hashCode() : 0);
        result = 31 * result + (shopAddress != null ? shopAddress.hashCode() : 0);
        return result;
    }
}
