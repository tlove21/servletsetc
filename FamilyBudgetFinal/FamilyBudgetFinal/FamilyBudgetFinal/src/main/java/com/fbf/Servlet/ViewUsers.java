package com.fbf.Servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "/viewusers", urlPatterns = {"/viewusers"})
public class ViewUsers extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");


        loginDao sp = loginDao.getInstance();


        final List<UsersEntity> listUsers = sp.getUsers();


        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Family Budget: View Users</title>");
            out.println("<meta charset=\"UTF-8\">\n" +
                    "    <meta name=\"description\" content=\"Family Budget Final\">\n" +
                    "    <meta name=\"keywords\" content=\"HTML,CSS,XML,JavaScript,JAVA\">\n" +
                    "    <meta name=\"author\" content=\"Trevon Morris\">\n" +
                    "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=0.86, maximum-scale=3.0, minimum-scale=0.86\">\n" +
                    "    <link rel=\"stylesheet\" href=\"css/normalize.css\">\n" +
                    "    <link rel=\"stylesheet\" href=\"css/styles.css\">");
            out.println("</head>");
            out.println("<body>");
            out.println("<header id=\"page-header\">\n" +
                    "    <div class=\"flexheader\">\n" +
                    "        <a href=\"../FamilyBudgetFinal_war\" class=\"logoimg\" title=\"Go to the PHP Motors Page\">\n" +
                    "        <img src=\"img/morrisfamilybudgetlogo.png\" alt=\"Morris Family logo\">\n" +
                    "        </a>\n" +
                    "    </div>\n" +
                    "</header>\n" +
                    "<nav class=\"topnav\" id=\"menu_nav\">\n" +
                    "    <!-- start links-->\n" +
                    "    <ul><li><a href=\"../FamilyBudgetFinal_war/addusers\">User Add</a></li> <li><a href=\"../FamilyBudgetFinal_war/addproducts\">Product Add</a></li><li><a href=\"../FamilyBudgetFinal_war/addshops\">Shop Add</a></li>\n" +
                    "        <li><a href=\"../FamilyBudgetFinal_war/viewusers\">View User</a></li><li><a href=\"../FamilyBudgetFinal_war/viewproducts\">View Products</a></li><li><a href=\"../FamilyBudgetFinal_war/viewshops\">View Shops</a></li></ul>\n" +
                    "    <!-- end of links-->\n" +
                    "</nav>");
            out.println("<main id=\"main\">");
            out.println("<section id=\"maincontent\">");
            out.println("<h4>User Listing:</h4>");
            out.println("<table width=\"100%\">");
            out.println("<tr><td>User Name</td><td>First Name</td><td>Last Name</td></tr>");
            listUsers.forEach((UsersEntity user) -> out.println("<tr><td>" + user.getUserName() + "</td>" + "<td>" + user.getFirstName() + "</td>" + "<td>" + user.getLastName() + "</td></tr>"));
            out.println("</table>");
            out.println("</section>");
            out.println("</main>");
            out.println("<footer id=\"footer\">\n" +
                    "    <p>&copy Morris Family Budget, All rights reserved<br>\n" +
                    "        All images used are believed to be in \"Fair Use\". Please notify the author if any are not and they will be removed.\n" +
                    "        Last Updated December 2020 </p>\n" +
                    "</footer>");
            out.println("</body>");
            out.println("</html>");
        }catch (IOException e){
            //Nothing to display not necessary, but just ensuring for testing only
            e.printStackTrace();
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.getRequestDispatcher("WEB-INF/templates/viewusers.jsp").forward(request, response);

    }
}
