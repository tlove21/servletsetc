package com.fbf.Servlet;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class ProductsDao {

    SessionFactory factory = null;
    Session session = null;

    private static ProductsDao single_instance = null;

    private ProductsDao()
    {
        factory = HibernateUtil.getSessionFactory();
    }

    /** This is what makes this class a singleton.  You use this
     *  class to get an instance of the class.
     * @return*/
    public static ProductsDao getInstance()
    {
        if (single_instance == null) {
            single_instance = new ProductsDao();
        }

        return single_instance;
    }

    /** Used to get more than one customer from database
     *  Uses the OpenSession construct rather than the
     *  getCurrentSession method so that I control the
     *  session.  Need to close the session myself in finally.*/
    public List<ProductsEntity> getProducts() {

        try {

            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.fbf.Servlet.ProductsEntity ORDER BY productShop";
            List<ProductsEntity> us = (List<ProductsEntity>) session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return us;

        } catch (HibernateException e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    public List<ProductsEntity> getProductShop(int sid) {

        try {

            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.fbf.Servlet.ProductsEntity where productShop=" + Integer.toString(sid) + "order by productName";
            List<ProductsEntity> us = (List<ProductsEntity>) session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return us;

        } catch (HibernateException e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    /** Used to get a single customer from database
     * @return*/
    public ProductsEntity getProduct(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.fbf.Servlet.ProductsEntity where id=" + Integer.toString(id);
            ProductsEntity u = (ProductsEntity) session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return u;

        } catch (HibernateException e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }
}
