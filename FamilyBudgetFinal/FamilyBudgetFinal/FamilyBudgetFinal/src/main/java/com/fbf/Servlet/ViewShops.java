package com.fbf.Servlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "/viewshops", urlPatterns = {"/viewshops"})
public class ViewShops extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");


        ShopsDao sp = ShopsDao.getInstance();


        final List<ShopsEntity> listShops = sp.getShops();

        ShopsDao s1name = ShopsDao.getInstance();





        try (PrintWriter out = response.getWriter()) {
            /* Page output created from header, footer and template body to load all needed styles */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Family Budget: View Shops</title>");
            out.println("<meta charset=\"UTF-8\">\n" +
                    "    <meta name=\"description\" content=\"Family Budget Final\">\n" +
                    "    <meta name=\"keywords\" content=\"HTML,CSS,XML,JavaScript,JAVA\">\n" +
                    "    <meta name=\"author\" content=\"Trevon Morris\">\n" +
                    "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=0.86, maximum-scale=3.0, minimum-scale=0.86\">\n" +
                    "    <link rel=\"stylesheet\" href=\"css/normalize.css\">\n" +
                    "    <link rel=\"stylesheet\" href=\"css/styles.css\">");
            out.println("</head>");
            out.println("<body>");
            out.println("<header id=\"page-header\">\n" +
                    "    <div class=\"flexheader\">\n" +
                    "        <a href=\"../FamilyBudgetFinal_war\" class=\"logoimg\" title=\"Go to the PHP Motors Page\">\n" +
                    "        <img src=\"img/morrisfamilybudgetlogo.png\" alt=\"Morris Family logo\">\n" +
                    "        </a>\n" +
                    "    </div>\n" +
                    "</header>\n" +
                    "<nav class=\"topnav\" id=\"menu_nav\">\n" +
                    "    <!-- start links-->\n" +
                    "    <ul><li><a href=\"../FamilyBudgetFinal_war/addusers\">User Add</a></li> <li><a href=\"../FamilyBudgetFinal_war/addproducts\">Product Add</a></li><li><a href=\"../FamilyBudgetFinal_war/addshops\">Shop Add</a></li>\n" +
                    "        <li><a href=\"../FamilyBudgetFinal_war/viewusers\">View User</a></li><li><a href=\"../FamilyBudgetFinal_war/viewproducts\">View Products</a></li><li><a href=\"../FamilyBudgetFinal_war/viewshops\">View Shops</a></li></ul>\n" +
                    "    <!-- end of links-->\n" +
                    "</nav>");
            out.println("<main id=\"main\">");
            out.println("<section id=\"maincontent\">");
            out.println("<h4>Shop Listing and Location:</h4>");
            out.println("<table width=\"100%\">");
            out.println("<tr><td>Shop</td><td>Location</td></tr>");
            listShops.forEach((ShopsEntity shopName) -> out.println("<tr><td>" + shopName.getShopName() + "</td>"+"<td>" + shopName.getShopAddress() + "</td></tr>"));
            out.println("</table>");
            out.println("</section>");
            out.println("</main>");
            out.println("<footer id=\"footer\">\n" +
                    "    <p>&copy Morris Family Budget, All rights reserved<br>\n" +
                    "        All images used are believed to be in \"Fair Use\". Please notify the author if any are not and they will be removed.\n" +
                    "        Last Updated December 2020 </p>\n" +
                    "</footer>");
            out.println("</body>");
            out.println("</html>");
        }catch (IOException e){
            //Nothing to display not necessary, but just ensuring for testing only
            e.printStackTrace();
        }

    }

    private void listShop(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ShopsDao sd = ShopsDao.getInstance();
        try {

            List<ShopsEntity> listShops = sd.getShops();
            request.setAttribute("listShops", listShops);

            RequestDispatcher dispatcher = request.getRequestDispatcher("addproducts.jsp");
            dispatcher.forward(request, response);
            return;

        } catch (IOException e) {
            e.printStackTrace();
            throw new ServletException(e);
        } catch (ServletException e) {
            e.printStackTrace();
            throw new ServletException(e);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("WEB-INF/templates/viewshop.jsp").forward(request, response);
    }
}
