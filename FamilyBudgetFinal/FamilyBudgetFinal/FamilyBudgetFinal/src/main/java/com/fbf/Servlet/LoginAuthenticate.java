package com.fbf.Servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "/login", urlPatterns = {"/login"})
public class LoginAuthenticate extends HttpServlet {

        private static final long serialVersionUID = 1L;
        private loginDao login;

    public void init() {
        login = new loginDao();
    }


        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException {

            request.getRequestDispatcher("WEB-INF/templates/login.jsp").forward(request, response);

        }

        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException {


            String username = request.getParameter("username");
            String password = request.getParameter("password");
            PrintWriter out = response.getWriter();

            if (login.validate(username, password)) {


                try {
                    /* TODO output page here.*/
                    out.println("<!DOCTYPE html>");
                    out.println("<html>");
                    out.println("<head>");
                    out.println("<title>Family Budget: Admin Area</title>");
                    out.println("<meta charset=\"UTF-8\">\n" +
                            "    <meta name=\"description\" content=\"Family Budget Final\">\n" +
                            "    <meta name=\"keywords\" content=\"HTML,CSS,XML,JavaScript,JAVA\">\n" +
                            "    <meta name=\"author\" content=\"Trevon Morris\">\n" +
                            "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=0.86, maximum-scale=3.0, minimum-scale=0.86\">\n" +
                            "    <link rel=\"stylesheet\" href=\"css/normalize.css\">\n" +
                            "    <link rel=\"stylesheet\" href=\"css/styles.css\">");
                    out.println("</head>");
                    out.println("<body>");
                    out.println("<header id=\"page-header\">\n" +
                            "    <div class=\"flexheader\">\n" +
                            "        <a href=\"../FamilyBudgetFinal_war\" class=\"logoimg\" title=\"Go to the PHP Motors Page\">\n" +
                            "        <img src=\"img/morrisfamilybudgetlogo.png\" alt=\"Morris Family logo\">\n" +
                            "        </a>\n" +
                            "    </div>\n" +
                            "</header>\n" +
                            "\n" +
                            "<nav class=\"topnav\" id=\"menu_nav\">\n" +
                            "    <!-- start links-->\n" +
                            "    <ul><li><a href=\"../FamilyBudgetFinal_war/addusers\">User Add</a></li> <li><a href=\"../FamilyBudgetFinal_war/addproducts\">Product Add</a></li><li><a href=\"../FamilyBudgetFinal_war/addshops\">Shop Add</a></li>\n" +
                            "        <li><a href=\"../FamilyBudgetFinal_war/viewusers\">View User</a></li><li><a href=\"../FamilyBudgetFinal_war/viewproducts\">View Products</a></li><li><a href=\"../FamilyBudgetFinal_war/viewshops\">View Shops</a></li></ul>\n" +
                            "    <!-- end of links-->\n" +
                            "</nav>");
                    out.println("<main id=\"main\">");
                    out.println("<section id=\"maincontent\">");
                    out.println("<h1>Admin Area</h1>");
                    out.println("<div align=\"center>\"");
                    out.println("<h1>You have logged in successfully:</h1> " + username);
                    out.println("<p>Welcome to the Admin Area, you may add users, shops, and products. You can also view all that exists in the database, by using the links that start with View</p>");
                    out.println("</div>");
                    out.println("</section>");
                    out.println("</main>");
                    out.println("<footer id=\"footer\">\n" +
                            "    <p>&copy Morris Family Budget, All rights reserved<br>\n" +
                            "        All images used are believed to be in \"Fair Use\". Please notify the author if any are not and they will be removed.\n" +
                            "        Last Updated December 2020 </p>\n" +
                            "</footer>");
                    out.println("</body>");
                    out.println("</html>");


                } finally {
                    out.close();
                }

            }else {

                /* TODO output page here.*/
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Family Budget: Admin Area</title>");
                out.println("<meta charset=\"UTF-8\">\n" +
                        "    <meta name=\"description\" content=\"Family Budget Final\">\n" +
                        "    <meta name=\"keywords\" content=\"HTML,CSS,XML,JavaScript,JAVA\">\n" +
                        "    <meta name=\"author\" content=\"Trevon Morris\">\n" +
                        "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=0.86, maximum-scale=3.0, minimum-scale=0.86\">\n" +
                        "    <link rel=\"stylesheet\" href=\"css/normalize.css\">\n" +
                        "    <link rel=\"stylesheet\" href=\"css/styles.css\">");
                out.println("</head>");
                out.println("<body>");
                out.println("<header id=\"page-header\">\n" +
                        "    <div class=\"flexheader\">\n" +
                        "        <a href=\"../FamilyBudgetFinal_war\" class=\"logoimg\" title=\"Go to the PHP Motors Page\">\n" +
                        "        <img src=\"img/morrisfamilybudgetlogo.png\" alt=\"Morris Family logo\">\n" +
                        "        </a>\n" +
                        "    </div>\n" +
                        "</header>\n" +
                        "\n" +
                        "<nav class=\"topnav\" id=\"menu_nav\">\n" +
                        "    <!-- start links-->\n" +
                        "    <ul> <li><a href=\"../FamilyBudgetFinal_war\">Home</a></li> <li><a href=\"../FamilyBudgetFinal_war/login\">Login</a></li></ul>\n" +
                        "    <!-- end of links-->\n" +
                        "</nav>");
                out.println("<main id=\"main\">");
                out.println("<section id=\"maincontent\">");
                out.println("<h1>Login Error</h1>");
                out.println("<div align=\"center>\"");
                out.println("<h1>Something went wrong</h1>");
                out.println("<p>Username or password is wrong: <a href=\"../FamilyBudgetFinal_war/login\">Try Again</a></li></ul></p>");
                out.println("</div>");
                out.println("</section>");
                out.println("</main>");
                out.println("<footer id=\"footer\">\n" +
                        "    <p>&copy Morris Family Budget, All rights reserved<br>\n" +
                        "        All images used are believed to be in \"Fair Use\". Please notify the author if any are not and they will be removed.\n" +
                        "        Last Updated December 2020 </p>\n" +
                        "</footer>");
                out.println("</body>");
                out.println("</html>");

            }
        }
}

