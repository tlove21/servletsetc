package com.fbf.Servlet;

import javax.persistence.*;

@Entity
@Table(name = "products", schema = "familybudgetfinal")
public class ProductsEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    private String productName;
    private float productPrice;
    private int productShop;

    public ProductsEntity(int id, String productName, float productPrice, int productShop) {
        this.id = id;
        this.productName = productName;
        this.productPrice = productPrice;
        this.productShop = productShop;
    }

    public ProductsEntity(String productName, float productPrice, int productShop) {
        this.productName = productName;
        this.productPrice = productPrice;
        this.productShop = productShop;
    }

    public ProductsEntity() {
    }

    public ProductsEntity(int productShop) {
    }


    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "ProductName", nullable = true, length = 45)
    public String getProductName() {
        return productName;
    }
    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Basic
    @Column(name = "ProductPrice", nullable = true, length = 45)
    public float getProductPrice() {
        return productPrice;
    }
    public void setProductPrice(float productPrice) {
        this.productPrice = productPrice;
    }

    @Basic
    @Column(name = "ProductShop", nullable = true)
    public int getProductShop() {
        return productShop;
    }
    public void setProductShop(int productShop) {
        this.productShop = productShop;
    }


    @Override
    public int hashCode() {
        return super.hashCode();
    }


    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
