package com.fbf.Servlet;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class ShopsDao {

    SessionFactory factory = null;
    Session session = null;

    private static ShopsDao single_instance = null;

    private ShopsDao()
    {
        factory = HibernateUtil.getSessionFactory();
    }

    /** This is what makes this class a singleton.  You use this
     *  class to get an instance of the class.
     * @return*/
    public static ShopsDao getInstance()
    {
        if (single_instance == null) {
            single_instance = new ShopsDao();
        }

        return single_instance;
    }

    /** Used to get more than one customer from database
     *  Uses the OpenSession construct rather than the
     *  getCurrentSession method so that I control the
     *  session.  Need to close the session myself in finally.*/
    public List<ShopsEntity> getShops() {

        try {

            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.fbf.Servlet.ShopsEntity ORDER BY shopName";
            List<ShopsEntity> us = (List<ShopsEntity>) session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return us;

        } catch (HibernateException e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    /** Used to get a single customer from database
     * @return*/
    public ShopsEntity getShop(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.fbf.Servlet.ShopsEntity where id=" + Integer.toString(id);
            ShopsEntity u = (ShopsEntity) session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return u;

        } catch (HibernateException e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }
}
