package com.fbf.Servlet;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class loginDao {

    SessionFactory factory = null;
    Session session = null;

    Transaction transaction = null;
    UsersEntity user;

    private static loginDao single_instance = null;

    public static loginDao getInstance()
    {
        if (single_instance == null) {
            single_instance = new loginDao();
        }

        return single_instance;
    }

    protected loginDao()
    {
        factory = HibernateUtil.getSessionFactory();
    }

    public List<UsersEntity> getUsers() {

        try {

            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.fbf.Servlet.UsersEntity ORDER BY firstName";
            List<UsersEntity> us = (List<UsersEntity>) session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return us;

        } catch (HibernateException e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    public boolean validate(String userName, String password) {


        try {

            session = factory.openSession();
            session.getTransaction().begin();

            user = (UsersEntity) session.createQuery("FROM com.fbf.Servlet.UsersEntity U WHERE U.userName = :userName").setParameter("userName", userName)
                    .uniqueResult();
            session.getTransaction().commit();


            if (user != null && user.getPassword().equals(password)) {
                return true;
            }
            // commit transaction
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return false;
    }
}
