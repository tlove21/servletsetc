<%--
  Created by IntelliJ IDEA.
  User: tlove
  Date: 12/5/2020
  Time: 11:17 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Family Budget: Add Shops</title>
    <meta charset="UTF-8">
    <meta name="description" content="Family Budget Final">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript,JAVA">
    <meta name="author" content="Trevon Morris">
    <meta name="viewport" content="width=device-width, initial-scale=0.86, maximum-scale=3.0, minimum-scale=0.86">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>

<%@include file="/common/headerlogin.jsp"%>

<main id="main">
    <section id="maincontent">
    <h2>Add Shop</h2>
    <form action="addshops" method="post">
        Shop Name: <input type="text" name="shopName" required/>
        Address: <input type="text" name="shopAddress" required/>
        <input type="submit" value="Submit"/>
    </form>
    </section>
</main>

<%@include file="/common/footer.jsp"%>

</body>
</html>
