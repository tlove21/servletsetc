<%--
  Created by IntelliJ IDEA.
  User: tlove
  Date: 12/5/2020
  Time: 11:17 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Family Budget: User Login</title>
    <meta charset="UTF-8">
    <meta name="description" content="Family Budget Final">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript,JAVA">
    <meta name="author" content="Trevon Morris">
    <meta name="viewport" content="width=device-width, initial-scale=0.86, maximum-scale=3.0, minimum-scale=0.86">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>

    <%@include file="/common/header.jsp"%>

    <main id="main">
        <section id="maincontent">
    <h1>Login Form</h1>
    <form action="<%=request.getContextPath()%>/login" method="post">

        <div class="form-group">
            <label for="username">User Name:</label> <input type="text"
                                                         class="form-control" id="username" placeholder="User Name"
                                                         name="username" required>
        </div>

        <div class="form-group">
            <label for="password">Password:</label> <input type="password"
                                                        class="form-control" id="password" placeholder="Password"
                                                        name="password" required>
        </div>


        <input type="submit" value="Submit"/>
    </form>
        </section>
</main>

<%@include file="/common/footer.jsp"%>

</body>
</html>
