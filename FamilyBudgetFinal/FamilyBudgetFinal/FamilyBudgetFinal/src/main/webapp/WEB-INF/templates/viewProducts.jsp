<%--
  Created by IntelliJ IDEA.
  User: tlove
  Date: 12/5/2020
  Time: 11:17 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Family Budget: View Products</title>
    <meta charset="UTF-8">
    <meta name="description" content="Family Budget Final">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript,JAVA">
    <meta name="author" content="Trevon Morris">
    <meta name="viewport" content="width=device-width, initial-scale=0.86, maximum-scale=3.0, minimum-scale=0.86">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>
<main>

    <%@include file="/common/headerlogin.jsp"%>

    <main id="main">
        <section id="maincontent">
    <h2>View Products</h2>
    <form action="viewproducts" method="post">

        Select Shop 1: <select name="shopEntityList">

                    <option value="1">Marcia's Wholesale</option>
                    <option value="2">Pace's Supermarket</option>
                    <option value="3">HiLo Supermarket</option>
                    <option value="4">Market A Wholesale</option>

        </select>

        Select Shop 2: <select name="shopEntityList2">

                    <option value="1">Marcia's Wholesale</option>
                    <option value="2">Pace's Supermarket</option>
                    <option value="3">HiLo Supermarket</option>
                    <option value="4">Market A Wholesale</option>
        </select>

        <input type="submit" value="Submit"/>
    </form>
        </section>
</main>

        <%@include file="/common/footer.jsp"%>

</body>
</html>
