<%--
  Created by IntelliJ IDEA.
  User: tlove
  Date: 12/5/2020
  Time: 11:17 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Family Budget: Add Users</title>
    <meta charset="UTF-8">
    <meta name="description" content="Family Budget Final">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript,JAVA">
    <meta name="author" content="Trevon Morris">
    <meta name="viewport" content="width=device-width, initial-scale=0.86, maximum-scale=3.0, minimum-scale=0.86">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>
<main>

    <%@include file="/common/headerlogin.jsp"%>

    <main id="main">
        <section id="maincontent">
    <h1>Add User</h1>
    <form action="addusers" method="post">
        User Name: <input type="text" name="username" required/>
        First Name: <input type="text" name="firstname" required/>
        Last Name : <input type="text" name="lastname" required/>
        Password  : <input type="password" name="password" required/>
        <input type="submit" value="Submit"/>
    </form>
        </section>

</main>
        <%@include file="/common/footer.jsp"%>
</body>
</html>
