<html lang="en">

<head>
    <title>Family Budget Final CIT 360</title>
    <meta charset="UTF-8">
    <meta name="description" content="Family Budget Final">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript,JAVA">
    <meta name="author" content="Trevon Morris">
    <meta name="viewport" content="width=device-width, initial-scale=0.86, maximum-scale=3.0, minimum-scale=0.86">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/styles.css">
</head>

<body>

<%@include file="/common/header.jsp"%>

<main id="main">
    <section id="maincontent">
        <h1>Welcome to the Morris Family Budget Portal!</h1>
        <p>The purpose of this portal is to compare shop prices from all stores the Morris Family shops at. This will help with budgeting, and this version is the first of a few more upgrades to come.</p>
        <p>Version 2 will have item calculator, update, and delete items</p>
        <p>Login: Add Admin:<a href="../FamilyBudgetFinal_war/login">visit</a></p>
    </section>

</main>
<%@include file="/common/footer.jsp"%>

</body>
</html>